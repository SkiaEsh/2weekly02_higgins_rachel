﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bulletBehavior : MonoBehaviour {

    //public bool collisionDetected = false;
    //Holds name of object hit.
    public string collisionObjName;

    // Use this for initialization
    void Start () {
		
	}

    void OnGUI()
    {
       // GUI.Label(new Rect(400, 0, 150, 150), "Collision Detected: " + collisionDetected.ToString() +
       //                                     '\n' + "Collision Object: " + collObjName);
    }

    // Update is called once per frame
    void Update () {
		
	}
    //Checks for collision and destroys the bullet if an asteroid is hit.
    void OnCollisionEnter2D(Collision2D collision)
    {
        collisionObjName = collision.gameObject.name;
        if (collision.gameObject.name == "asteroid(Clone)")
        {
            Destroy(gameObject);
        }

    }
}
