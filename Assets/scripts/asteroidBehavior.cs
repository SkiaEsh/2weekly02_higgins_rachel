﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class asteroidBehavior : MonoBehaviour
{
    //public bool collisionDetected = false;
    //Gets the name of the object collided to see if action should be taken.
    public string collisionObjName;
    //Holds the explosion objects.
    public GameObject explosion;

    void OnGUI()
    {
      //  GUI.Label(new Rect(200, 0, 150, 150), "Collision Detected: " + collisionDetected.ToString() +
      //                                     '\n' + "Collision Object: " + collisionObjName);
    }

    // Use this for initialization
    void Start()
    {

    }

	// Update is called once per frame
	void Update ()
    {
		//Asteroid will reappear on opposite side of the screen.
        if (gameObject.name == "asteroid(Clone)")
        {
            // X-axis limit that creates a toroidal effect, causing the asteroids to loop around to the other side.
            if (gameObject.GetComponent<Rigidbody2D>().position.x <= -9.0f)
            {
                gameObject.transform.position = new Vector2(9.0f, gameObject.transform.position.y);
            }
            else if (gameObject.GetComponent<Rigidbody2D>().position.x >= 9.0f)
            {
                gameObject.transform.position = new Vector2(-9.0f, gameObject.transform.position.y);
            }

            // Y-axis limit that works similarly to the X-Axis one.
            if (gameObject.GetComponent<Rigidbody2D>().position.y <= -5.2f)
            {
                gameObject.transform.position = new Vector2(gameObject.transform.position.x, 5.2f);
            }
            else if (gameObject.GetComponent<Rigidbody2D>().position.y >= 5.2f)
            {
                gameObject.transform.position = new Vector2(gameObject.transform.position.y, -5.2f);
            }
        }
	}
    //Collision detection.  It sets an explosion graphic to an asteroid that collided with the bullet and destroys the asteroid.
    void OnCollisionEnter2D(Collision2D collision)
    {
        collisionObjName = collision.gameObject.name;
        if (collision.gameObject.name == "bullet(Clone)")
        {
            GameObject explosionFade = (Instantiate(explosion, gameObject.transform.position, Quaternion.identity)) as GameObject;
            Destroy(gameObject);
        }

    }
}
