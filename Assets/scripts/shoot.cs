﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class shoot : MonoBehaviour
{
    //Speed holds the speed propelling the bullets.
    public float speed = 200;
    //Counts the number of bullets.
    public int counter = 0;
    //Keeps track of time to limit bullets shot with cannotShoot.
    public float timer = Time.time + 5;
    public bool cannotShoot = false;
    //public bool collisionDetected = false;
    //public string reloadTest = "N";
   // public string bulletTag;
    //Game objects hold the generated bullets and the ship, respectively.
    //Bullets holds multiple bullets which are shot from the front position if the ship.
    public GameObject bullet;
    public GameObject ship;
    //Bullet clones generated when space is pressed.
    public GameObject newBullet;
    //Gets the audio source for the shot sound.
    private AudioSource shootingSound;

    // Use this for initialization
    void Start()
    {
        //reloadTest = "S";
        //Sets the audio source for the shot sound.
        shootingSound = GetComponent<AudioSource>();
    }

    void OnGUI()
    {
       // GUI.Label(new Rect(0, 0, 150, 150), "Shots: " + counter.ToString() + 
       //                                     '\n' + "Shoot Flag: " + cannotShoot.ToString() + 
       //                                     '\n' + "Hit Reload: " + reloadTest.ToString() + 
       //                                     '\n' + "Collision: " + collisionDetected.ToString() +
       //                                     '\n' + "Bullet Tag: " + bulletTag);
    }

    // Update is called once per frame
    void Update()
    {
        if (cannotShoot == false)
        {
            //Uses space bar to shoot.
            if (Input.GetKeyDown(KeyCode.Space))
            {
                //Counter is iterated and checked at start; if it put after the shot occurs,
                //the program might not reach it in time, allowing it to shoot more than four
                //bullets per second.
                counter++;
                if (counter >= 4)
                {
                    cannotShoot = true;
                }

                //creates a new bullet object at the ship's position, then takes Z-position and adds it with the Y-position multiplied by a negative
                //to move the bullet forward from the center to the front of the ship.  Identity quaternion is used for rotation.
                newBullet = (Instantiate(bullet, ship.transform.position + transform.forward + ship.transform.up * -0.45f, Quaternion.identity)) as GameObject;
                //Takes the ship's upward position and multiplies it by a negative to make it shoot in the right direction.  Impulse is
                //used to propel the bullet.  The bullets are detroyed to preventing slowdown and crashing awhile after they spawn.
               // bulletTag = newBullet.name;
                newBullet.GetComponent<Rigidbody2D>().AddForce(-1 * ship.transform.up * speed, ForceMode2D.Impulse);
                //Sound effect loads as bullet appears.
                shootingSound.Play();

                //Destroys bullet instances after two seconds to prevent overloading the game.
                Destroy(newBullet, 2f);
            }
        }
        //If one second has passed, the counter goes back to 0, cannotShoot is set to false
        //to allow shooting again, and the timer is iterated by 1.
        if (Time.time > timer)
        {
            counter = 0;
            cannotShoot = false;
            timer = Time.time + 1;
        }
        //Destroys bullet instances after two seconds to prevent overloading the game.
        Destroy(newBullet, 2.0f);
    }

}
